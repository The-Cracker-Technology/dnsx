rm -rf /opt/ANDRAX/bin/dnsx

make

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make... PASS!"
else
  # houston we have a problem
  exit 1
fi

strip dnsx

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Strip... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf dnsx /opt/ANDRAX/bin/dnsx

chown -R andrax:andrax /opt/ANDRAX/bin/dnsx
chmod -R 755 /opt/ANDRAX/bin/dnsx
